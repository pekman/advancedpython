import requests
from datetime import date, timedelta
import datetime
import pandas as pd
from collections import defaultdict
import pickle
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages
import difflib

def download():
    start_date = date(2000, 1, 1)   # start date
    end_date = date(2021, 8, 3)
    #end_date = date.today()   # end date
    delta = end_date - start_date       # as timedelta

    for i in range(delta.days + 1):
        day = start_date + timedelta(days=i)
        print(day)
        url = 'https://seb.se/pow/fmk/2100/fonder_'+str(day)+'.TXT'
        data = requests.get(url, allow_redirects=True)
        data_file = open('./data/'+str(day)+'.txt', 'wb')
        data_file.write(data.content)
        data_file.close()

def makeCSV():
    start_date = date(2013, 1, 1)
    end_date = date(2021, 8, 3)
    #end_date = date.today()
    delta = end_date - start_date

    df = pd.DataFrame()
    df_list = []
    for i in range(delta.days + 1):
        day = start_date + timedelta(days=i)
        #print(day)
        try:
            df_list.append((pd.read_csv("./data/"+str(day)+".txt",encoding = "ISO-8859-1",delimiter=";",header=None)))
        except pd.errors.ParserError:
            continue
            
    merged_df = pd.concat(df_list)
    #list_of_names = list(set(merged_df[1].tolist()))
    #for name in list_of_names:
    #    print(name,":",difflib.get_close_matches(name,list_of_names,n=5),"\n")
    #print(difflib.get_close_matches("Robur Bas Mix",list_of_names,n=5))
    #print(df[0])
    #print(merged_df.loc[(merged_df[1] == "Swedbank Robur Bas Mix") | (merged_df[1] == "Robur Bas Mix")])
    merged_df.to_csv("./merged_df.csv", sep=';', encoding='utf-8',index=False,header=["date","name","price","ticker"])

def makeDict():
    df = pd.read_csv("merged_df.csv", sep=';', encoding='utf-8',low_memory=False)#[:1000]
    #print(df["name"])
    #table = pd.pivot_table(df, index=["date","price"],columns=["name"])
    massive_dict = defaultdict(list)
    for index, row in df.iterrows():
        try:
            massive_dict[str(row["ticker"])+"_date"].append( datetime.datetime.strptime(row["date"],"%Y-%m-%d"))
            massive_dict[str(row["ticker"])+"_price"].append(float(row["price"].replace(",",".")))
        except ValueError:
            print("error:", row["ticker"])
    
    with open('massive_dict.pickle', 'wb') as handle:
        pickle.dump(massive_dict, handle, protocol=pickle.HIGHEST_PROTOCOL)
    
    
    #df["type"] = df.apply(lambda row: type(row[1]), axis=1)
    #print("FMG/Alterum Gl Quality" == df[1].loc[[1]])
    #print("FMG/Alterum Gl Quality" in df[1])
    
    
    
    #print(("FMG/Alterum Gl Quality") in df)
    #df = df[1]["SEB Aktiesparfond"]
    #print(df)

def run():
    with open('massive_dict.pickle', 'rb') as handle:
        massive_dict = pickle.load(handle)
        #print(massive_dict)
        #print(massive_dict)
        
        fig, ax = plt.subplots(figsize=(800/65, 600/65), dpi=65,sharex=True)
        
        i=0
        for key in massive_dict:
            #if(("_date" in key) or ("SEB" in key) or ("Robur" not in key)):
            if(("_date" in key)):
                continue
            else:
                #print(key)
                ax.plot(massive_dict[key.split("_")[0]+"_date"], massive_dict[key.split("_")[0]+"_price"] , label=key.split("_")[0])
            i+=1
        #leg = ax.legend(borderpad=0.5, frameon=False, loc=2)
        print(i)
            
        ax.grid()
        
        ax.set_xlabel("Time [yrs]", ha='right', x=1.0)
        ax.set_ylabel("Fund Balance [kr]", ha='right', y=1.0)
        
        ax.set_ylim(0,4000)
        ax.set_xlim(date(2013, 1, 1),date(2022, 1, 1))
        
        plt.tight_layout()
        #plt.show()
        with PdfPages("./correnta.pdf") as pdf:
            pdf.savefig()


def main():
    #download()
    #makeCSV()
    #makeDict()
    #run()
    #df = pd.read_csv("merged_df.csv", sep=';', encoding='utf-8',low_memory=False)#[:1000]
    #df.groupby(["ticker"])
    #print(df)

#print(df_list[0])
#print(df_list[1])
#merged_df = df_list[0].append(df_list[1])
#print(merged_df)

if __name__ == "__main__":
    main()

